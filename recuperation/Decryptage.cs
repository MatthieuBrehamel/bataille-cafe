﻿using System;
using System.Net; // pour les outils rÃ©seau
using System.Net.Sockets; // pour les sockets
using System.Text;

namespace Bataille_cafe
{
    class Decryptage
    {

        /*
         * Decrypte la trame, la rends comprehensible (10x10 avec caractères ascii des groupes).
         * ( S3 : GOOD++ )
         *  A ne pas toucher.
         */
        public static Parcelle[,] Decryptage_trame(string Trame)
        {
                int[,] tabTemp = new int[10, 10];
                tabTemp = Decoupage_trame(Trame);
                Parcelle[,] tab_map_char = Frontiere_type(tabTemp);
                Initialisation_tab_char(tab_map_char);
                tab_map_char = Association_lettre(tab_map_char);

            return tab_map_char;
        }


        /*
         * Decoupe la trame reçu 
         * ( S3 : GOOD )
         */
        public static int[,] Decoupage_trame(string trame)
        {
            int[,] tab_map_int = new int[10, 10];//Tableau à 2 dimensions
            string[] ligne_map_int = new string[10];
            string[] colonne_map_int = new string[10];

            ligne_map_int = trame.Split('|'); // Split les lignes
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)
            {
                colonne_map_int = ligne_map_int[indice_ligne].Split(':'); // Split les valeurs/colonnes
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)
                {
                    //Console.WriteLine("tabCol : "+ colonne_map_int[indice_colonne]);
                    
                    tab_map_int[indice_ligne, indice_colonne] = int.Parse(colonne_map_int[indice_colonne]);
                    
                }
            }
            return tab_map_int;
        }

        /*
         * Determine le type des parcelles et les frontieres des groupes 
         * ( S3 : GOOD )
         */
        public static Parcelle[,] Frontiere_type(int[,] tab_map_int)
        {
            Parcelle[,] tab_map_int_front = new Parcelle[10, 10];

            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    string type = "Parcelle de Terre";
                    bool Nord = false, Sud = false, Est = false, Ouest = false;
                    int parcelleActuelle = tab_map_int[indice_ligne, indice_colonne]; // Commence à 0
                    
                    if (parcelleActuelle >= 15)
                    {
                        if(parcelleActuelle >= 32 && parcelleActuelle < 64)

                        {
                            parcelleActuelle = parcelleActuelle - 32;
                            type = "Parcelle de Foret";
                        }
                        if(parcelleActuelle >= 64)
                        {
                            parcelleActuelle = parcelleActuelle - 64;
                            type = "Parcelle de Mer";
                        }
                    }

                    if (parcelleActuelle >= 8)
                    {
                        parcelleActuelle = parcelleActuelle - 8;
                        Est = true;
                    }
                    if (parcelleActuelle >= 4)
                    {
                        parcelleActuelle = parcelleActuelle - 4;
                        Sud = true;
                    }
                    if (parcelleActuelle >= 2)
                    {
                        parcelleActuelle = parcelleActuelle - 2;
                        Ouest = true;
                    }
                    if (parcelleActuelle == 1)
                    {
                        parcelleActuelle = 0;
                        Nord = true;
                    }
                    tab_map_int_front[indice_ligne, indice_colonne] = new Parcelle(type,Nord, Sud, Ouest, Est,false, false, false,""); //Creation de Parcelle
                }
            }
            return tab_map_int_front;
        }

        /*
        * Attributs une lettre par groupe
         * ( S3 : GOOD )
        */
        public static Parcelle[,] Association_lettre(Parcelle[,] tab_map_int_front)
        {
            char caractere_actuel = 'a';
            // PARCOURS TAB
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if (tab_map_int_front[indice_ligne, indice_colonne].Type == "Parcelle de Mer") //Si la parcelle actuelle est une Mer
                    {
                        tab_map_int_front[indice_ligne, indice_colonne].Groupe = 'M'; // Alors le groupe est M
                    }
                    if (tab_map_int_front[indice_ligne, indice_colonne].Type == "Parcelle de Foret")
                    {
                        tab_map_int_front[indice_ligne, indice_colonne].Groupe = 'F';
                    }
                    if (tab_map_int_front[indice_ligne, indice_colonne].Type == "Parcelle de Terre" && tab_map_int_front[indice_ligne, indice_colonne].Groupe == 'X')
                    {
                        tab_map_int_front[indice_ligne, indice_colonne].Groupe = caractere_actuel;
                        Association_groupe(indice_ligne, indice_colonne, tab_map_int_front, caractere_actuel);
                        caractere_actuel++;
                    }

                }
            }
            return tab_map_int_front;
        }

        /*
        * Associe les valeurs des frontières à une valeurs de groupe pour chaque parcelle de terre.
        * ( S3 : GOOD )
        */
        public static void Association_groupe(int indice_ligne, int indice_colonne, Parcelle[,] parcelleActuelle, char caractere_actuel)
        {

            if (parcelleActuelle[indice_ligne, indice_colonne].FNord == false)
            {
                if (parcelleActuelle[(indice_ligne - 1), indice_colonne].Groupe == 'X')
                {
                    parcelleActuelle[(indice_ligne - 1), indice_colonne].Groupe = caractere_actuel;
                    Association_groupe((indice_ligne - 1), indice_colonne, parcelleActuelle, caractere_actuel);
                }
            }

            if (parcelleActuelle[indice_ligne, indice_colonne].FSud == false)
            {
                if (parcelleActuelle[(indice_ligne + 1), indice_colonne].Groupe == 'X')
                {
                    parcelleActuelle[(indice_ligne + 1), indice_colonne].Groupe = caractere_actuel;
                    Association_groupe((indice_ligne + 1), indice_colonne, parcelleActuelle, caractere_actuel);
                }
            }
            if (parcelleActuelle[indice_ligne, indice_colonne].FOuest == false)
            {
                if (parcelleActuelle[indice_ligne, (indice_colonne - 1)].Groupe == 'X')
                {
                    parcelleActuelle[indice_ligne, (indice_colonne - 1)].Groupe = caractere_actuel;
                    Association_groupe(indice_ligne, (indice_colonne - 1), parcelleActuelle, caractere_actuel);
                }
            }
            if (parcelleActuelle[indice_ligne, indice_colonne].FEst == false)
            {
                if (parcelleActuelle[indice_ligne, (indice_colonne + 1)].Groupe == 'X')
                {
                    parcelleActuelle[indice_ligne, (indice_colonne + 1)].Groupe = caractere_actuel;
                    Association_groupe(indice_ligne, (indice_colonne + 1), parcelleActuelle, caractere_actuel);
                }
            }
        }
        /*
         * Initialise toutes les valeurs du tableau de caractères de la "carte" à 'X'
         * ( S3 : GOOD )
         */
        public static void Initialisation_tab_char(Parcelle[,] tab_map_char)
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    tab_map_char[indice_ligne, indice_colonne].Groupe = 'X';
                }
            }

        }

        
    }
}
