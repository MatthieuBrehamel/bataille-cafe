﻿using System;
using System.Collections.Specialized;
using System.Net; // pour les outils rÃ©seau
using System.Net.Sockets; // pour les sockets
using System.Text;

namespace Bataille_cafe
{
    class Affichage : Serveur
    {

        /*
        * Affiche la carte via un parcours avec 2 boucles for imbriques
        * 
        * Vide | Jaune : 0
        * Parcelle Serveur | Rouge : 1 
        * Parcelle Joueur | Vert : 2
        * Parcelle Foret/Mer | Vert/Bleu | Police Blanche : 8
        * Police verte : .CoupsOpti = Respecte les regles
        */

        public static void Affichage_Carte(Parcelle[,] tab_map_char) 
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if (tab_map_char[indice_ligne, indice_colonne].Groupe == 'F') //Met en Vert les Forets
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Green;
                    }
                    else if (tab_map_char[indice_ligne, indice_colonne].Groupe == 'M') //Met en Bleu les Mers
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    else if (tab_map_char[indice_ligne, indice_colonne].Parcelle_serveur) //Met en Rouge si le SERVEUR a joue sur cette parcelle
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Red;
                    }
                    else if (tab_map_char[indice_ligne, indice_colonne].Parcelle_joueur) //Met en Vert si le SERVEUR a joue sur cette parcelle
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Green;
                    }
                    else                                                                  //Met en Orange les parcelles vides
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.DarkYellow;
                    }
                    if (tab_map_char[indice_ligne, indice_colonne].Coups_opti.Equals(indice_ligne+""+indice_colonne)) //Met la police en vert si la parcelle respecte les regles
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        tab_map_char[indice_ligne, indice_colonne].Coups_opti = "";

                    }
                    Console.Write(tab_map_char[indice_ligne, indice_colonne].Groupe); // Ecris la case
                    Console.Write(" "); // Espace entre chaque case
                    
                }
                Console.WriteLine(""); //Retour a la ligne tout les 10 cases

            }
            Console.ForegroundColor = ConsoleColor.White; //Remet les couleurs par defaut
            Console.BackgroundColor = ConsoleColor.Black;
        }






    }
}
