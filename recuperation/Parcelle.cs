﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bataille_cafe
{
    public class Parcelle
    {
        private char groupe; // caractere du groupe
        private bool fNord; 
        private bool fSud;
        private bool fOuest;
        private bool fEst;
        private string type;
        private bool libre;
        private string coups_opti; 
        private bool parcelle_joueur;
        private bool parcelle_serveur;

        public string Coups_opti { set => coups_opti = value; get => coups_opti; }
        public bool Parcelle_joueur { set => parcelle_joueur = value; get => parcelle_joueur; }
        public bool Parcelle_serveur { set => parcelle_serveur = value; get => parcelle_serveur; }
        public bool Libre { set => libre = value; get => libre; }
        public char Groupe { set => groupe = value; get => groupe; }
        public bool FNord { get => fNord; }
        public bool FSud { get => fSud; }
        public bool FOuest { get => fOuest; }
        public bool FEst { get => fEst; }
        public string Type { get => type; set => type = value; }

        public Parcelle(string Type, bool fNord, bool fSud, bool fOuest, bool fEst,bool libre,bool parcelle_joueur, bool parcelle_serveur,string coups_opti)
        {
            this.Type = Type; 
            this.fNord = fNord;
            this.fSud = fSud;
            this.fOuest = fOuest;
            this.fEst = fEst;
            this.libre = libre;
            this.parcelle_joueur = parcelle_joueur;
            this.parcelle_serveur = parcelle_serveur;
            this.coups_opti = coups_opti;
        }


    }

}
