﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net; // pour les outils rÃ©seau
using System.Net.Sockets; // pour les sockets
using System.Text;

namespace Bataille_cafe
{
    class Program : Serveur
    {

        public static void Main(string[] args)
        {
            bool Finish = false; // Variable permettant que la partie finisse proprement
            string trame = Serveur.Connexion_Recuperation(); // Etabli la connexion avec le serv & recupère la trame
            Parcelle[,] plateau = Decryptage.Decryptage_trame(trame); // Decrypte la trame reçu en tableau à 2dim
            Affichage.Affichage_Carte(plateau); //Affiche au debut le plateau decrypte vide.
            
            string case_Joueur = TraitementIA.Premier_Coup(plateau); // Premier coup de l'IA
            string coup_Joueur = "A:" + case_Joueur; // Transforme le coup au format conforme

            do
            {

                Serveur.Serveur_Jouer(coup_Joueur); //Fais le coup demander
                Console.WriteLine("Vous avez joue : '{0}'", coup_Joueur);
                if (Serveur.ValeurFini) { Finish = true; break; } //Fini la boucle
                TraitementIA.envoieValeurs(case_Joueur); //Envois les valeurs au tableau
                TraitementIA.AttributionValeurs_Carte(plateau);
                Affichage.Affichage_Carte(plateau); //Reaffiche


                List<char> distinct = TraitementIA.groupeListe.Distinct().ToList(); //Supprime doublons
                TraitementIA.TriGroupesListe(plateau, distinct); // RANGER DANS L'ORDRE DE TAILLE (6>3>4>2)



                foreach (char value in TraitementIA.groupeListe_Trie) //Parcours des groupes valide à etre joue
                {
                    if (TraitementIA.Avantage_Allie(plateau, value) != "NO"){ // Priorise l'avantage allie
                        
                        case_Joueur = TraitementIA.Avantage_Allie(plateau, value);
                    }
                    else if (TraitementIA.Groupes_Preference(plateau, value) != "NO") // Sinon joue par rapport à la preference groupe
                    {
                        case_Joueur = TraitementIA.Groupes_Preference(plateau, value);
                    }
                    coup_Joueur = "A:" + case_Joueur; // met le format attendu par le serveur 

                }
                TraitementIA.groupeListe.Clear(); // Reset list

            } while (!Finish); // Tant que le serveur n'envoi pas "FINI" 

            Serveur.Deconnexion(); // Deconnecte la socket avec le serveur.


        }

    }
}
