﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net; // pour les outils rÃ©seau
using System.Net.Sockets; // pour les sockets
using System.Text;

namespace Bataille_cafe
{
    class TraitementIA : Serveur
    {

        // Variables globales
        public static List<char> groupeListe = new List<char>(); // Liste des groupes jouable
        public static List<char> groupeListe_Trie = new List<char>(); // Liste des groupes jouable trié dans l'ordre (6>3>4>2)
        static int[,] tab_map_envoie = new int[10, 10]; // Tableau des cases jouees
        static int Colonne;
        static int Ligne;
        public static string Groupes_opti="";


        /*
         *  Parcours et attribution de valeurs aux parcelles interessantes.
         *  (S4 : GOOD)
         */
        public static void AttributionValeurs_Carte(Parcelle[,] tab_map_char)
        {

            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {

                    if (tab_map_char[indice_ligne, indice_colonne].Groupe == 'F')
                    {
                        tab_map_envoie[indice_ligne, indice_colonne] = 8;
                    }
                    else if (tab_map_char[indice_ligne, indice_colonne].Groupe == 'M')
                    {
                        tab_map_envoie[indice_ligne, indice_colonne] = 8;
                    }
                    else if (tab_map_envoie[indice_ligne, indice_colonne] == 1) //Si Serveur a joue
                    {
                        tab_map_char[indice_ligne, indice_colonne].Libre = true;
                        tab_map_char[indice_ligne, indice_colonne].Parcelle_serveur = true;
                    }
                    else if (tab_map_envoie[indice_ligne, indice_colonne] == 2) //Si Joueur a joue
                    {
                        tab_map_char[indice_ligne, indice_colonne].Libre = true;
                        tab_map_char[indice_ligne, indice_colonne].Parcelle_joueur = true;
                    }


                    // Respecte les regles de base
                    if ((indice_ligne == Ligne || indice_colonne == Colonne) && tab_map_envoie[indice_ligne, indice_colonne] != 8 && tab_map_envoie[indice_ligne, indice_colonne] != 1 && tab_map_envoie[indice_ligne, indice_colonne] != 2 && tab_map_char[indice_ligne, indice_colonne].Groupe != tab_map_char[Ligne, Colonne].Groupe)
                    {
                        tab_map_char[indice_ligne, indice_colonne].Coups_opti = indice_ligne + "" + indice_colonne;
                        groupeListe.Add(tab_map_char[indice_ligne, indice_colonne].Groupe);
                    }
                }
            }

        }


        /*
         * Transmet les choix des joueurs au tableaux pour une meilleure comprehension du jeu
         * (S4 : GOOD)
         */
        public static void envoieValeurs(string case_Joueur)
        {
            // Coups Joueur (IA)
            Ligne = int.Parse(case_Joueur[0].ToString());
            Colonne = int.Parse(case_Joueur[1].ToString());
            tab_map_envoie[Ligne, Colonne] = 2;

            // Coups Serveur
            Ligne = int.Parse(trameRecu2[2].ToString());
            Colonne = int.Parse(trameRecu2[3].ToString());
            tab_map_envoie[Ligne, Colonne] = 1;
            
        }


        /*
         * Tri la liste des groupes jouable en priorite ( 6 -> 3 -> 4 -> 2 ).
         * ( S4 :GOOD)
         */
        public static void TriGroupesListe(Parcelle[,] tab_map_char, List<char> distinct)
        {

            List<char> groupe6 = new List<char>();
            List<char> groupe3 = new List<char>();
            List<char> groupe4 = new List<char>();
            List<char> groupe2 = new List<char>();
            groupeListe_Trie.Clear();
            groupe6.Clear();
            groupe3.Clear();
            groupe4.Clear();
            groupe2.Clear();
            foreach (char value in distinct)
            {
                if (tailleGroupes(tab_map_char, value) == 6)
                {
                    groupe6.Add(value);
                }
                else if (tailleGroupes(tab_map_char, value) == 3)
                {
                    groupe3.Add(value);
                }
                else if (tailleGroupes(tab_map_char, value) == 4)
                {
                    groupe4.Add(value);
                }
                else if (tailleGroupes(tab_map_char, value) == 2)
                {
                    groupe2.Add(value);
                }
            }
            foreach (char v in groupe6)
            {
                groupeListe_Trie.Add(v);
            }
            foreach (char v in groupe3)
            {
                groupeListe_Trie.Add(v);
            }
            foreach (char v in groupe4)
            {
                groupeListe_Trie.Add(v);
            }
            foreach (char v in groupe2)
            {
                groupeListe_Trie.Add(v);
            }
        }

        ///////////////////////////////////////////////////////////////
        //////////////////// STRATEGIES DE JEU  ///////////////////////
        ///////////////////////////////////////////////////////////////

        /*
         * Jouera toujours la premiere parcelle faisant partie d'un groupe de 6
         * (S4: GOOD)
         */
        public static string Premier_Coup(Parcelle[,] tab_map_char)
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if (tailleGroupes(tab_map_char, tab_map_char[indice_ligne,indice_colonne].Groupe) == 6) //Si la taille du groupe est de 6
                    {
                        return indice_ligne + "" + indice_colonne;
                    }
                }
            }
            return "NO";
        }

        /*
         * Permet de jouer par rapport à l'avantage allié d'un groupe de parcelles Afin de valider les points de celle ci
         * (S4 : GOOD)
         */
        public static string Avantage_Allie(Parcelle[,] tab_map_char, char groupe)
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    // Stop à moitie+1 , faut qu'il y ait plus de parcelles Allie que d'enemies et que ca soit une parcelle parmis les valide vertes
                    if ( (tailleGroupes(tab_map_char,groupe)/2) > CompteurParcellesAllies(tab_map_char,groupe) &&(CompteurParcellesAllies(tab_map_char, groupe) > CompteurParcellesEnnemies(tab_map_char, groupe)) && tab_map_char[indice_ligne,indice_colonne].Groupe == groupe && ((indice_ligne == Ligne || indice_colonne == Colonne) && tab_map_envoie[indice_ligne, indice_colonne] != 8 && tab_map_envoie[indice_ligne, indice_colonne] != 1 && tab_map_envoie[indice_ligne, indice_colonne] != 2 && tab_map_char[indice_ligne, indice_colonne].Groupe != tab_map_char[Ligne, Colonne].Groupe))
                    {
                        //Avantage de parcelles
                        return indice_ligne + "" + indice_colonne;
                    }
                }
            }
            return "NO";
        }

        public static string Groupes_Preference(Parcelle[,] tab_map_char, char groupe)
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if ((indice_ligne == Ligne || indice_colonne == Colonne) && tab_map_envoie[indice_ligne, indice_colonne] != 8 && tab_map_envoie[indice_ligne, indice_colonne] != 1 && tab_map_envoie[indice_ligne, indice_colonne] != 2 && tab_map_char[indice_ligne, indice_colonne].Groupe != tab_map_char[Ligne, Colonne].Groupe)
                    {
                        if (tailleGroupes(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) == 6 && CompteurParcellesAllies(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) < 4)
                        {
                            return indice_ligne + "" + indice_colonne;
                        }
                        else if (tab_map_char[indice_ligne, indice_colonne].Groupe == groupe && CompteurParcellesAllies(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) < 2 && tailleGroupes(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) == 3)
                        {
                            return indice_ligne + "" + indice_colonne;
                        }
                        else if (tab_map_char[indice_ligne, indice_colonne].Groupe == groupe && tailleGroupes(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) == 4)
                        {
                            return indice_ligne + "" + indice_colonne;
                        }
                        else if (tailleGroupes(tab_map_char, tab_map_char[indice_ligne, indice_colonne].Groupe) == 2)
                        {
                            return indice_ligne + "" + indice_colonne;
                        }
                    }

                }
            }
            return "NO";
        }

        //////////////////////////////////////////////////////
        //////////////////// COMPTEURS ///////////////////////
        //////////////////////////////////////////////////////
        
        /*
         * Determine la taille d'un groupe
         * (GOOD)
         */
        public static int tailleGroupes(Parcelle[,] tab_map_char,char groupe)
        {
            int compteurParcelles_totale = 0;

            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if(tab_map_char[indice_ligne, indice_colonne].Groupe == groupe)
                    {
                        compteurParcelles_totale++;
                    }
                }
            }
            return compteurParcelles_totale;
        }

        /*
         * Compte le nombre de parcelles allié dans un groupe donne
         * (S4 : GOOD)
         */
        public static int CompteurParcellesAllies(Parcelle[,] tab_map_char, char groupe)
        {
            int CompteurParcellesAllies = 0;

            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if (tab_map_char[indice_ligne, indice_colonne].Parcelle_joueur && tab_map_char[indice_ligne, indice_colonne].Groupe == groupe)
                    {
                        CompteurParcellesAllies++;
                    }
                }
            }
            return CompteurParcellesAllies;
        }

        /*
         * Compte le nombre de parcelles ennemie dans un groupe donne
         * (S4 : GOOD)
         */
        public static int CompteurParcellesEnnemies(Parcelle[,] tab_map_char, char groupe)
        {
            int CompteurParcellesEnnemies = 0;

            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++)// Parcours ligne
            {
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++)// Parcours des valeurs de la ligne
                {
                    if (tab_map_char[indice_ligne, indice_colonne].Parcelle_serveur && tab_map_char[indice_ligne, indice_colonne].Groupe == groupe)
                    {
                        CompteurParcellesEnnemies++;
                    }
                }
            }
            return CompteurParcellesEnnemies;
        }



    }
}
