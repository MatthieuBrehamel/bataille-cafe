﻿using System;
using System.Net; // pour les outils rÃ©seau
using System.Net.Sockets; // pour les sockets
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Bataille_cafe
{
    class Serveur 
    {
        public static Socket tempSocket;
        public static bool ValeurFini = false;
        private static bool Connecte = false;
        private static string trameRecu = "";
        public static string trameRecu2 = "";
        public static string trameRecu3 = "";

        public static string Connexion_Recuperation()
        {
            
            //Try-catch des Exeptions pouvant arriver
            try
            {
                if(Connecte == false)
                {
                    //IPAddress ip = Dns.GetHostEntry("172.16.0.88").AddressList[0];
                    //CrÃ©ation d'un point de terminaison du rÃ©seau avec l'adresse IP et le Port du serveur
                    IPEndPoint ipe = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1213);

                    //CrÃ©ation d'une variable tempSocket de type Socket qui prend un nouvel objet Socket 
                    // qui contient le type de communication, le type de flux et le type de protocle utilisÃ©s
                    tempSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    //Ã©tablissement de la connexion grÃ¢ce Ã  la Socket et le point de terminaison
                    tempSocket.Connect(ipe);
                    Connecte = true;
                }

                //dÃ©claration du Buffer qui va recevoir la trame
                byte[] receivedBuffer = new byte[1000];
                
                //Ã©criture dans le buffer de la trame reÃ§u par la Socket
                tempSocket.Receive(receivedBuffer);

                //convertion des bytes reÃ§u en string,  dans un tableau de string  
                System.Text.Encoding.ASCII.GetString(receivedBuffer).ToCharArray();
                string[] tabDonnees = { ASCIIEncoding.Default.GetString(receivedBuffer) };

                //insertion de la trame dans une variable string 
                trameRecu = tabDonnees[0];

                //desactivation de la Socket apres bonne reception de la trame

                //Fermeture pour liberation des ressources
                return trameRecu;

            }
            catch (System.Net.Sockets.SocketException ex)
            {
                //Ecriture dans la console du message d'erreur
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
            

        }

        public static void Deconnexion() // Deconnexion du client (socket) à l'hote distant.
        {
            if (Connecte == true)
            {
                try
                {
                    tempSocket.Shutdown(SocketShutdown.Both);
                    tempSocket.Close();
                    Connecte = false;
                    Console.WriteLine("La deconnexion a ete effectuee");
                }
                catch (SocketException error)
                {
                    Console.WriteLine($"Problème deconnexion : {error}");
                }
                catch (ObjectDisposedException error2)
                {
                    Console.WriteLine($"Ce socket n'existe plus {error2}");
                }
            }
            else
            {
                Console.WriteLine("Ce socket n'est pas coonecte");
            }
        }



        public static string Serveur_Jouer(string coup)
        {
            byte[] sendBuffer;
            try
            {
            byte[] envoieDonnees = Encoding.UTF8.GetBytes(coup);
            sendBuffer = envoieDonnees;

            tempSocket.Send(sendBuffer);

            //dÃ©claration du Buffer qui va recevoir la trame
            byte[] receivedBuffer2 = new byte[4];
            
            //Ã©criture dans le buffer de la trame reÃ§u par la Socket
            tempSocket.Receive(receivedBuffer2);
            System.Text.Encoding.ASCII.GetString(receivedBuffer2).ToCharArray();
            string[] Validation = { ASCIIEncoding.Default.GetString(receivedBuffer2) };
            
            tempSocket.Receive(receivedBuffer2);
            System.Text.Encoding.ASCII.GetString(receivedBuffer2).ToCharArray();
            string[] coupsServeur = { ASCIIEncoding.Default.GetString(receivedBuffer2) };

            tempSocket.Receive(receivedBuffer2);
            System.Text.Encoding.ASCII.GetString(receivedBuffer2).ToCharArray();
            string[] Continuer = { ASCIIEncoding.Default.GetString(receivedBuffer2) };

            //Transmission de la valeurFini pour stopper la boucle de jeu dans le main
            if(coupsServeur[0].Equals("FINI") || Continuer[0].Equals("FINI"))
                {
                    ValeurFini = true;
                }

            //Debogage (optionnel)
             if (Validation[0] == "VALI")
             {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine(Validation[0]);
             }
             else
             {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(Validation[0]);
             }
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;




            //convertion des bytes reÃ§u en string,  dans un tableau de string 

            //insertion de la trame dans une variable string 
            trameRecu2 = coupsServeur[0];
            trameRecu3 = Continuer[0];
                //desactivation de la Socket apres bonne reception de la trame

                //Fermeture pour liberation des ressources


                return trameRecu2;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                //Ecriture dans la console du message d'erreur
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }


    }
}
